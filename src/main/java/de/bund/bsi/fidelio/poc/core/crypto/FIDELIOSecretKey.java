/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.core.crypto;

import java.math.BigInteger;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.ECParameterSpec;
import java.util.function.Supplier;

import javax.crypto.SecretKey;


/**
 * This class provides a default secret key implementation for use within FIDELIO. The format is fixed to "RAW". The
 * code herein implements a lazy-get, clone and zeroize-source functionality.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */
public class FIDELIOSecretKey implements SecretKey, ECPrivateKey {

	private static final long serialVersionUID = 1L;

	private transient Supplier<byte[]> keyFunc;
	private final String alg;


	/**
	 * Create an instance from a key supplier.
	 *
	 * @param key
	 *            function returning a byte-array as key.
	 */
	public FIDELIOSecretKey(final Supplier<byte[]> key) {
		this.keyFunc = key;
		this.alg = "RAW";
	}


	/**
	 * @see SecretKey#getAlgorithm()
	 */
	@Override
	public String getAlgorithm() {
		return this.alg;
	}


	/**
	 * @see SecretKey#getFormat()
	 */
	@Override
	public String getFormat() {
		return this.alg;
	}


	/**
	 * @see SecretKey#getEncoded()
	 */
	@Override
	public byte[] getEncoded() {
		final byte[] thisKey = this.keyFunc.get();
		final byte[] resultKey = thisKey.clone();

		destroy();
		for (int i = 0; i < thisKey.length; i++) {
			thisKey[i] = 0x00;
		}

		return resultKey;
	}


	/**
	 * Utility function to convert the key into a big-integer for elliptic curve operations.
	 *
	 * @return big-integer representing the key
	 */

	@Override
	public BigInteger getS() {
		return new BigInteger(1, getEncoded());
	}


	/**
	 * @see SecretKey#destroy()
	 */
	@Override
	public void destroy() {
		this.keyFunc = null;
	}


	/**
	 * @see SecretKey#isDestroyed()
	 */
	@Override
	public boolean isDestroyed() {
		return this.keyFunc == null;
	}


	/**
	 * This class does not store curve parameters.
	 *
	 * @return always null
	 * @see SecretKey#getParams
	 */

	@Override
	public ECParameterSpec getParams() {
		return null;
	}
}
