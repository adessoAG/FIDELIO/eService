/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.core.u2f;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.List;


/**
 * This class provides utility functions to encode U2F token response messages.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */
public class U2FCodec {

	/**
	 * Concatenate the arrays in list order.
	 *
	 * @param src
	 *            list of byte arrays
	 * @return concatenated arrays as a single array
	 */
	public static byte[] concat(final List<byte[]> src) {
		byte[] result = null;
		for (final byte[] b : src) {
			result = concat(result, b);
		}
		return result;
	}


	/**
	 * Concatenate the arrays in argument order.
	 *
	 * @param src
	 *            vargars / array of byte arrays
	 * @return concatenated arrays as a single array
	 */
	public static byte[] concat(final byte[]... src) {
		if (src == null) {
			return null;
		}

		if (src.length == 0) {
			return new byte[0];
		}

		int totalLen = 0;
		for (final byte[] b : src) {
			if (b != null && b.length > 0) {
				totalLen += b.length;
			}
		}

		final ByteBuffer bb = ByteBuffer.allocate(totalLen);
		for (final byte[] b : src) {
			if (b != null && b.length > 0) {
				bb.put(b);
			}
		}

		return bb.array();
	}


	/**
	 * Create data to be signed for U2F enroll response.
	 *
	 * @param appIdHash
	 *            hash of application ID
	 * @param clientDataHash
	 *            hash of client data structure
	 * @param keyHandle
	 *            created key-handle
	 * @param userPubKey
	 *            created public key matching key-handle
	 * @return input to token signature algorithm
	 */
	public static byte[] enrollSigned(final byte[] appIdHash, final byte[] clientDataHash,
			final byte[] keyHandle, final byte[] userPubKey) {
		return concat(new byte[] { 0 }, appIdHash, clientDataHash, keyHandle, userPubKey);
	}


	/**
	 * Create U2F enroll response.
	 *
	 * @param userPubKey
	 *            created public key matching key-handle
	 * @param keyHandle
	 *            created key-handle
	 * @param cert
	 *            token attestation certificate
	 * @param signature
	 *            token attestation signature
	 * @return U2F enroll data according to FIDO U2F 1.x specs
	 * @throws GeneralSecurityException
	 */
	public static byte[] enrollData(final byte[] userPubKey, final byte[] keyHandle,
			final X509Certificate cert, final byte[] signature) throws GeneralSecurityException {
		final byte[] enrollResponse = concat(new byte[] { 5 }, userPubKey, new byte[] { (byte) keyHandle.length },
				keyHandle, cert.getEncoded(), signature);
		return enrollResponse;
	}


	/**
	 * Create data to be signed for U2F authenticate / sign-in response.
	 *
	 * @param appIdHash
	 *            hash of application ID
	 * @param up
	 *            user presence verification flag
	 * @param i
	 *            signature counter value
	 * @param clientDataHash
	 *            hash of client data structure
	 * @return input to token signature algorithm
	 */
	public static byte[] authSigned(final byte[] appIdHash, final byte up, final int i,
			final byte[] clientDataHash) {
		final byte[] signedData = new byte[appIdHash.length + 1 + 4 + clientDataHash.length];
		ByteBuffer.wrap(signedData).put(appIdHash).put(up).putInt(i).put(clientDataHash);
		return signedData;
	}


	/**
	 * Create U2F authenticate / sign-in response
	 *
	 * @param up
	 *            user presence verification flag
	 * @param i
	 *            signature counter value
	 * @param signature
	 *            user key signature
	 * @return U2F authenticate data according to FIDO U2F 1.x specs
	 */
	public static byte[] authData(final byte up, final int i, final byte[] signature) {
		final byte[] authResponse = new byte[1 + 4 + signature.length];
		ByteBuffer.wrap(authResponse).put(up).putInt(i).put(signature);
		return authResponse;
	}
}
