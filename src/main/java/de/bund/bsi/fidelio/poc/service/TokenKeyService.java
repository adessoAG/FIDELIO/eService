/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.service;

import java.io.IOException;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.bund.bsi.fidelio.poc.core.crypto.FIDELIOSecretKey;


/**
 * This interface describes the required objects and methods to implement a service providing key material for token
 * operations from external input.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */

public interface TokenKeyService {

	/**
	 * Create a token key service context with a realm.
	 *
	 * @param realm
	 *            description of context
	 * @return token key service context
	 */
	public Context create(final String realm);


	/**
	 * Create a token key service context with a realm bound to a servle request.
	 *
	 * @param realm
	 *            description of context
	 * @param request
	 *            servlet request bound to the realm and context
	 * @return token key service context
	 */
	public Context create(final String realm, final HttpServletRequest request);


	// candidate for removal
	// public Context create(final String realm, final Object ctxInfo);

	/**
	 * Finishes and returns the context with the identifier <b>id</b>.
	 *
	 * @param id
	 *            context identifier
	 * @return token key service context associated to <b>id</b>
	 */
	public Context finish(final String id);


	/**
	 * This interface describes the necessary functionality to keep the state of a token key service context.
	 *
	 * @author Christian Kahlo, 2017
	 * @version $Id$
	 */
	interface Context {

		/**
		 * Connects the context to a specific application ID hash and binds it to a servlet response and re-entry URL.
		 *
		 * @param appIdHash
		 *            hash of application ID
		 * @param response
		 *            servlet response bound to this context
		 * @param reentryUrl
		 *            URL base for re-rentry to this context used as a prefix for redirections
		 * @return an implementation dependent object, may be null
		 * @throws IOException
		 */
		Object connect(final byte[] appIdHash, final HttpServletResponse response, final String reentryUrl)
				throws IOException;


		/**
		 * Internal method to set the key diversification function.
		 *
		 * @param fKDF
		 */
		abstract void setDiv(Function<byte[], byte[]> fKDF);


		/**
		 * Return an attribute related to the value of <b>key</b>.
		 *
		 * @param key
		 *            attribute identifier
		 * @return value of attribute, null if not existent
		 */
		public Object get(final String key);


		/**
		 * Set an attribute <b>value</b> with the identifier of <b>key</b>.
		 *
		 * @param key
		 * @param value
		 */
		public void put(final String key, final Object value);


		/**
		 * Key expansion function that generates FIDELIO secret key object depending on value of counter <b>ctr</b> and
		 * diversification data <b>divData</b>.
		 *
		 * @param ctr
		 *            counter value for diversification of keys
		 * @param divData
		 *            additional diversification data
		 * @return secret key for MAC or signature operations
		 */
		public FIDELIOSecretKey fKX(short ctr, byte[]... divData);

	}
}
