/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPrivateKey;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import javax.crypto.Mac;

import de.bund.bsi.fidelio.poc.core.crypto.CryptoProfile;
import de.bund.bsi.fidelio.poc.core.crypto.CryptoProfile.Type;
import de.bund.bsi.fidelio.poc.core.crypto.FIDELIOSecretKey;
import de.bund.bsi.fidelio.poc.core.format.FIDELIORequest;
import de.bund.bsi.fidelio.poc.core.format.FIDELIOResponse;
import de.bund.bsi.fidelio.poc.core.u2f.U2FCodec;
import de.bund.bsi.fidelio.poc.service.TokenKeyService.Context;
import de.persoapp.core.util.ArrayTool;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;


/**
 * This class implements the core FIDELIO eService functionality.
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */

public class FIDELIO {

	// size of random component in key
	private static final int R_SIZE = 32;

	private final CryptoProfile cp;
	private final Properties fidoProps;

	private final ECPrivateKey attKey;
	private final X509Certificate attCert;

	private final Function<byte[], Integer> globalCounter;

	// 0xF1 0xDE 0x11 0x01
	private static final byte[] FIDELIO_TAG = new byte[] { (byte) 0xF1, (byte) 0xDE, (byte) 0x11, (byte) 0x01 };


	/**
	 * Create an instance of this class with properties <b>fidoProps</b>.
	 *
	 * @param fidoProps
	 *            the properties for this service
	 */
	public FIDELIO(final Properties fidoProps) {
		if (fidoProps == null) {
			throw new NullPointerException("properties == null");
		}

		this.cp = CryptoProfile.getInstance(Type.P256); // make configurable?

		this.fidoProps = fidoProps;

		final RedisClient redisClient = RedisClient.create((String) this.fidoProps.get("redisServer"));
		final StatefulRedisConnection<String, String> rc = redisClient.connect();
		final RedisAsyncCommands<String, String> ac = rc.async();
		final String redisCounterPrefix = (String) this.fidoProps.get("redisCounterPrefix");
		final int redisCounterOrder = tryParseInt((String) this.fidoProps.get("redisCounterOrder"), 256);
		// use 256 (0 - 255) counters by default

		this.globalCounter = (keyHandle) -> {
			// calculate the sum of all bytes, could be anything distributing key handles across some numberspace
			try {
				int kHsum = 0;
				if(redisCounterOrder <= 32 * 255) { // legacy for dev and test-system with O(256), O(1024)
					for (final byte b : keyHandle) {
						kHsum += b & 0xFF;
					}

					return ac.incr(redisCounterPrefix + (kHsum % redisCounterOrder)).get().intValue();
				} else { // support for O(65536), O(262144) and O(16777216)
					for (final byte b : keyHandle) {
						kHsum = Integer.rotateLeft(kHsum, Byte.SIZE);
						kHsum ^= b & 0xFF;
					}

					return ac.incr(redisCounterPrefix + Integer.toHexString(kHsum % redisCounterOrder)).get().intValue();
				}
			} catch (final ExecutionException | InterruptedException ee) {
				ee.printStackTrace();
				return null;
			}
		};

		try (FileInputStream fis = new FileInputStream(this.fidoProps.getProperty("attestationKeystore", "keystore"))) {
			final KeyStore p12 = KeyStore.getInstance("PKCS12");
			p12.load(fis, this.fidoProps.getProperty("attestationPassword", "").toCharArray());

			final String firstAlias = p12.aliases().nextElement();
			this.attKey = (ECPrivateKey) p12.getKey(firstAlias,
					this.fidoProps.getProperty("attestationPassword", "").toCharArray());

			this.attCert = (X509Certificate) p12.getCertificate(firstAlias);
		} catch (GeneralSecurityException | IOException e) {
			throw new IllegalStateException(e);
		}
	}


	private int tryParseInt(final String number, final int defaultValue) {
		if (number != null && !number.isEmpty()) {
			try {
				return Integer.parseInt(number.trim());
			} catch (final NumberFormatException nfe) {
			}
		}
		return defaultValue;
	}


	/**
	 * Handle FIDO request.
	 *
	 * @param ctx
	 *            token key service context
	 * @param fidoReq
	 *            incoming FIDO U2F request
	 * @return outgoing FIDO response
	 * @throws GeneralSecurityException
	 */

	public FIDELIOResponse doFIDOeID(final Context ctx, final FIDELIORequest fidoReq)
			throws GeneralSecurityException {
		return doFIDOeID(ctx, fidoReq.getCommand(), fidoReq.getAppIdHash(),
				fidoReq.getClientDataHash(), fidoReq.getKeyHandles());
	}


	/**
	 * Handle FIDO request.
	 *
	 * @param ctx
	 *            token key service context
	 * @param command
	 *            FIDO U2F command
	 * @param appIdHash
	 *            application ID hash
	 * @param clientDataHash
	 *            client data hash
	 * @param keyHandles
	 *            list of key handles for blacklisting at enrollment and whitelisting at sign-in
	 * @return outgoing FIDO response
	 * @throws GeneralSecurityException
	 */
	public FIDELIOResponse doFIDOeID(final Context ctx, final byte command, final byte[] appIdHash,
			final byte[] clientDataHash, final List<byte[]> keyHandles)
			throws GeneralSecurityException {

		// RandomnessExtraction and secret initialization
		final Mac mac = this.cp.mac(ctx.fKX((short) 1));
		final Function<byte[], FIDELIOSecretKey> fP = (x) -> ctx.fKX((short) 2, x);

		if (appIdHash != null && appIdHash.length == 32 && clientDataHash != null && clientDataHash.length == 32) {
			final byte[] keyHandle = find1stHandle(keyHandles, mac); // find first matching key handle

			switch (command) {
			case 1: // enroll
				if (keyHandle != null) { // check for valid keyHandles on blacklist
					return respondError(-1, null, keyHandle);
				}

				// build a "FIDELIO1" tagged nonce, add inner MAC to verify eID <-> keyhandle relationship
				final byte[] tagWithNonce = U2FCodec.concat(FIDELIO_TAG, this.cp.rand(R_SIZE));
				final byte[] newKeyHandle = U2FCodec.concat(tagWithNonce, mac.doFinal(tagWithNonce));

				return respondEnroll(0, appIdHash, clientDataHash, this.cp.pubKey(fP.apply(newKeyHandle)), newKeyHandle);

			case 2: // authenticate / sign-in
				if (keyHandle != null) {
					return respondSign(0, appIdHash, clientDataHash, this.globalCounter.apply(keyHandle),
							fP.apply(keyHandle), keyHandle);
				}

				return respondError(-1, null, null); // no matching keyHandle

			case 3: // getVersion
			case 0: // RFU, ping?
			default: // unknown
				// what to do?
			}
		}

		throw new RuntimeException("Invalid request.");
	}


	/**
	 * Find first matching key handle. Checks key handles for size and correct tag and verifies HMAC.
	 *
	 * @param keyHandles
	 *            list of candidate key handles
	 * @param mac
	 *            the HMAC function for verification
	 * @return the value of the first matched key handle
	 */
	private byte[] find1stHandle(final List<byte[]> keyHandles, final Mac mac) {
		if (keyHandles != null && keyHandles.size() > 0) {
			for (final byte[] keyHandle : keyHandles) {
				if (keyHandle != null && keyHandle.length == FIDELIO_TAG.length + R_SIZE + mac.getMacLength() &&
						Arrays.equals(FIDELIO_TAG, ArrayTool.sub(keyHandle, 0, FIDELIO_TAG.length))) {
					mac.update(keyHandle, 0, FIDELIO_TAG.length + R_SIZE);
					if (Arrays.equals(ArrayTool.sub(keyHandle, FIDELIO_TAG.length + R_SIZE, mac.getMacLength()),
							mac.doFinal())) {
						return keyHandle; // key handle matched
					}
				}
			}
		}

		return null;
	}


	/**
	 * Create FIDELIO enrollment response.
	 *
	 * @param status
	 *            FIDO status, 0 = OK
	 * @param appIdHash
	 *            application ID hash of the incoming request
	 * @param clientDataHash
	 *            client data hash of the incoming request
	 * @param userPubKey
	 *            newly created user public key
	 * @param newKeyHandle
	 *            newly created key handle for the user public key
	 * @return FIDO U2F response object
	 * @throws GeneralSecurityException
	 */
	private FIDELIOResponse respondEnroll(final int status, final byte[] appIdHash, final byte[] clientDataHash,
			final byte[] userPubKey, final byte[] newKeyHandle) throws GeneralSecurityException {
		return new FIDELIOResponse((byte) status, U2FCodec.enrollData(userPubKey, newKeyHandle, this.attCert,
				this.cp.sign(this.attKey, U2FCodec.enrollSigned(appIdHash, clientDataHash, newKeyHandle, userPubKey))),
				new byte[0]);
	}


	/**
	 * Create FIDELIO sign-in response.
	 *
	 * @param status
	 *            FIDO status, 0 = OK
	 * @param appIdHash
	 *            application ID hash of the incoming request
	 * @param clientDataHash
	 *            client data hash of the incoming request
	 * @param ctr
	 *            signature counter for key handle
	 * @param key
	 *            user private key
	 * @param keyHandle
	 *            matched key handle for sign-in
	 * @return FIDO U2F response object
	 * @throws GeneralSecurityException
	 */

	private FIDELIOResponse respondSign(final int status, final byte[] appIdHash, final byte[] clientDataHash,
			final int ctr, final FIDELIOSecretKey key, final byte[] keyHandle) throws GeneralSecurityException {
		return new FIDELIOResponse((byte) status, U2FCodec.authData((byte) 1, ctr,
				this.cp.sign(key, U2FCodec.authSigned(appIdHash, (byte) 1, ctr, clientDataHash))),
				keyHandle);
	}


	/**
	 * Create FIDELIO error response. This response is sent if a key handle is already registered during enrollment or
	 * if no valid key handle has been found during authentication.
	 *
	 * @param status
	 *            != 0 means error
	 * @param data
	 *            optional associated data, maybe null
	 * @param keyHandle
	 *            optional associated key handle, may be null
	 * @return FIDO U2F response object
	 */

	private FIDELIOResponse respondError(final int status, final byte[] data, final byte[] keyHandle) {
		return new FIDELIOResponse((byte) -1, data, keyHandle);
	}
}
