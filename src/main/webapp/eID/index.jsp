<%@ page language="java" import="java.net.*" session="false"%><%
	final String origin = request.getHeader("origin"), baseURL = "https://" + request.getServerName()
	    + (request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) + request.getContextPath(),
        ua = request.getHeader("user-agent").toLowerCase();

	System.out.println("ORIGIN: " + origin);
	System.out.println("REFERER: " + request.getHeader("referer"));

	String location = URLEncoder.encode(baseURL + "/?" + request.getQueryString());
	if (ua.matches("^.*?(android).*$")) { // Android
	    location = "intent://127.0.0.1:24727/eID-Client?tcTokenURL=" + location + "#Intent;scheme=eid;B.android.intent.extra.RETURN_RESULT=true;S.browser_fallback_url=http%3A%2F%2Fid1.vx4.eu;end";
	    //location = "eid://127.0.0.1:24727/eID-Client?tcTokenURL=" + location + "&consume";
	} else if (ua.matches("^.*?(iphone|ipad|ipod).*$")) { // Apple
	    location = "eid://127.0.0.1:24727/eID-Client?tcTokenURL=" + location + "&consume";
	} else { // Desktop
	    location = "http://127.0.0.1:24727/eID-Client?tcTokenURL=" + location;
	}

	response.setStatus(303);
	response.setHeader("Access-Control-Allow-Origin", "*"); //origin);
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Referrer-Policy", "origin");
	response.setHeader("Location", location);
%>
